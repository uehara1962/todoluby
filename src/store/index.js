import { createStore } from 'redux'

const INITIAL_STORE = {
  data: [
  ]
}


const itens = (state=INITIAL_STORE, action) => {
  switch (action.type) {
    case 'ADD_ITEM':
      return { data: [...state.data, action.item]}
    case 'ADD_ITENS':
      return { data: [...state.data, ...action.itens]}
    case 'ORDER_ITENS':
      return { data: [...state.data.sort()]}
    default:
      return state
  }
}

const store = createStore(itens)

export default store