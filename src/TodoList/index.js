import React, {useRef, useState, useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import * as Yup from 'yup'

import {
  Tooltip,
  ListDiv,
  TooltipDiv
} 
from './styles'


function addItemAction(item){
  return { type: 'ADD_ITEM', item}
}

function addItensAction(itens){
  return { type: 'ADD_ITENS', itens}
}

function orderItensAction(){
  return { type: 'ORDER_ITENS' }
}

export default function TodoList (){
  const [ validationErrors, setValidationErrors ] = useState({
    descricao_item: '',
  })

  const lista = useSelector(
    state => state.data
  )
  
  const dispatch = useDispatch()
  const inputRef = useRef()


  useEffect(() => {
    console.log('useEffect_localStorage_get')
    const itens = JSON.parse(localStorage.getItem('todoList'))
    if(itens){
      console.log(itens)
      dispatch(addItensAction(itens))
    }
  }, [dispatch] )

  useEffect(() => {
    console.log('useEffect_localStorage_save')
    // localStorage.removeItem('todoList')
    localStorage.setItem('todoList', JSON.stringify(lista) )
  }, [lista] )


  function ListItem(){
    return (
      <ListDiv>
        <button onClick={() => {OrderList()}} >Ordenar</button>
        <ul>
        {
          lista.map((item,i) => <li key={i}>{item}</li>)
        }
        </ul>
      </ListDiv>
    )    
  }

  function OrderList(){
    console.log('OrderList')
    dispatch(orderItensAction())
  }


  async function addItem(){
    const dados = {
      [inputRef.current.name ]: inputRef.current.value,
  }
    console.log('addItem_dados :', dados)
    setValidationErrors({
      descricao_item: '',
    })
    try {
      const schema = Yup.object().shape({
        descricao_item: Yup
          .string()
          .required()
          .min(5, 'o item deve ter no mínimo 5 caracteres')
      })

      await schema.validate(dados, {
        abortEarly: false,
      })
      console.log('addItem_valido')
      // dispatch(updateItemRequest(inputRef.current.value))
      dispatch(addItemAction(dados['descricao_item']))
    } catch (err){
      const validationErrors = {}

      if (err instanceof Yup.ValidationError){
        err.inner.forEach(error => {
          // validationErrors[error.path] = error.message
          console.log('addItem_error: ', error)
          setValidationErrors({...validationErrors, [error.path]: error.message})
        })
        console.log('addItem_error: ', validationErrors)
      }
    }
  }  



  function FormAddItem(){
    return (
      <>
        <form 
          onSubmit={addItem}
        >
          <input 
            type="text"
            name="descricao_item"
            ref={inputRef}
          />
          <button type="submit">Add Item</button>
        </form>
          <TooltipDiv>
            {
              validationErrors.descricao_item && <Tooltip>{validationErrors.descricao_item}</Tooltip>
            }
          </TooltipDiv>
      </>
    )
  }

  return (
    <>
      <FormAddItem />
      <ListItem />
    </>
  )

  }