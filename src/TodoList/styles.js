import styled from 'styled-components'

export const Tooltip = styled.span`
  width: 300px;
  background-color: black;
  color: red;
  font-size: 13px;
  text-align: center;
  border-radius: 3px;
  padding: 5px 0;
  position: absolute;
  z-index: 1;
  top: 45px;
`;

export const ListDiv = styled.div`
  margin-top: 30px;
`;

export const TooltipDiv = styled.div`
  margin-top: 10px;
  width: 200px
`;